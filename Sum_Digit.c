#include <stdio.h>
int inp()
{
	int x;
	printf("Enter a number ");
	scanf("%d",&x);
	return x;
}
int sum(int n)
{
	int s=0,d;
	while(n!=0)
	{
		d=n%10;
		s+=d;
		n/=10;
	}
	return s;
}
void output(int n,int s)
{
	printf("Sum of digits of %d is %d",n,s);
}
int main()
{
	int n,s;
	n=inp();
	s=sum(n);
	output(n,s);
	return 0;
}