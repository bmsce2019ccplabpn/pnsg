#include<stdio.h>
#include<math.h>
struct point
{
    float x,y;
};
typedef struct point cordi;
    cordi input()
    {
        cordi p;
        printf("enter the abcissa\n");
        scanf("%f",&p.x);
        printf("enter the ordinate\n");
        scanf("%f",&p.y);
        return p;
    }
 

float dista(struct point p1,struct point p2)
{
    float d;
    d=sqrt(((p1.x - p2.x)*(p1.x-p2.x))+((p1.y - p2.y)*(p1.y-p2.y)));
    return d;
}

void output(float d)
{
    printf("the distance=%f\n",d);
}

int main()
{
    struct point p1,p2;
    float distance;
    p1=input();
    p2=input();
    distance=dista(p1,p2);
    output(distance);
    return 0;
}